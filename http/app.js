(function() {

  var app = window.app = {};

  function init() {
    app.loading = true;
    fetchHosts().done((response) => {
      let {hosts} = response;

      let rpi2 = app.rpi2 = hosts.find((current) => {
        return current.name == 'rpi2';
      });
      rpi2.streamUrl = `http://${rpi2.host}:${rpi2.port}/?action=stream`
      $('#video-stream-rpi2').attr('src', app.rpi2.streamUrl);

      app.loading = false;
    });

    $('#btn-left').on('click', (sendLeft));
    $('#btn-right').on('click', sendRight);
    $('#btn-up').on('click', sendUp);
    $('#btn-down').on('click', sendDown);
    $('#btn-center').on('click', sendCenter);
    $('#pwr-on').on('click', sendPwrOn);
    $('#pwr-off').on('click', sendPwrOff);
    $('#rpi2-start-stream').on('click', () => {
      startStream('rpi2');
    });
    $('#rpi2-stop-stream').on('click', () => {
      stopStream('rpi2');
    });
    $('#cooler-off').on('click', switchOffCooler);
    $('#cooler-on').on('click', switchOnCooler);
  };


  function fetchHosts() {
    return $.get('/hosts');
  }

  function sendLeft(event) {
    $.get('/left');
    event.preventDefault();
  }
  function sendRight(event) {
    $.get('/right');
    event.preventDefault();
  }
  function sendUp(event) {
    $.get('/up');
    event.preventDefault();
  }
  function sendDown(event) {
    $.get('/down');
    event.preventDefault();
  }
  function sendCenter(event) {
    $.get('/center');
    event.preventDefault();
  }
  function sendPwrOn(event) {
    $.get('/pwr-on');
    event.preventDefault();
  }
  function sendPwrOff(event) {
    $.get('/pwr-off');
    event.preventDefault();
  }
  function startStream(hostName) {
      hostName = hostName || 'rpi2';
      $.get(`/start-stream?hostName=${hostName}`).then(() => {
        $(`#video-stream-${hostName}`).css('display', 'inline');
        // update image url in order to reload
        let uniqueUrl = app[hostName] ? (app[hostName].streamUrl + '?t=' + new Date().getTime() ) : '';
        $(`#video-stream-${hostName}`).attr('src', uniqueUrl );
      });
  }
  function stopStream(hostName) {
      hostName = hostName || 'rpi2';
      $.get(`/stop-stream?hostName=${hostName}`).then(() => {
        $(`#video-stream-${hostName}`).css('display', 'none');
      });
  }

  function switchOnCooler() {
    $.get('/cooler-on');
    event.preventDefault();
  }
  function switchOffCooler() {
    $.get('/cooler-off');
    event.preventDefault();
  }

  $(init);

})();
