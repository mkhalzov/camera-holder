const raspi = require('raspi-io');
const five = require('johnny-five');
const board = new five.Board({
        io: new raspi(),
        repl: false
});

const proxy = require('./scripts/proxy');
const servoCtrl = require('./scripts/servo-ctrl');
const pwrCtrl = require('./scripts/power-ctrl.js');
const coolerCtrl = require('./scripts/cooler-ctrl.js');

board.on("ready", function() {

    const gate = new five.Pin({
        pin: "GPIO4"
    });

    const axisX = new five.Servo({
        pin: "GPIO19"
    });
    const axisY = new five.Servo({
        pin: "GPIO18",
        range: [30,140]
    });

    const coolerGate = new five.Pin({
        pin: "GPIO26"
    });

    try {
        pwrCtrl.init({ gate });
        servoCtrl.init({ axisX, axisY });
        coolerCtrl.init({ gate: coolerGate });
    } catch (e) {
        console.error(e);
    };

    proxy.start();

    /*
    this.repl.inject({
        axisX, axisY,
        servoCtrl,
        proxy,
        pwrCtrl,
        coolerGate
    });
    */
});
