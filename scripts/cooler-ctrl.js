/**
 * N-chanel transistor controller
 */

const five = require("johnny-five");

var exports = module.exports = {};
Object.assign(exports, {
    init,
    on,
    off
});

var gate;

function init(config) {
    if (!config) {
        throw new Error('missing init configuration for the cooler controller');
    };

    if (!config.gate instanceof five.Pin) {
        throw new Error('missing config.gate');
    };
    ( { gate } = config );
    off();
}

function on(){
    gate.high();
}

function off(){
    gate.low();
}

