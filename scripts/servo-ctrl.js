const five = require('johnny-five');

var exports = module.exports = {};
Object.assign(exports, {
    init,
    center,
    left,
    right,
    up,
    down
});

const stepAngle = 10;
const stepTime = 100;

var x, y;

function init(config) {
    if (!config) {
        throw new Error('missing init configuration for the servo controller');
    };
    if (!(config.axisX instanceof five.Servo) || !(config.axisY instanceof five.Servo)) {
        throw new Error('missing x.axis and/or y.axis in the init configuration');
    };
    let { axisX, axisY } = config;

    x = {
        servo: axisX,
        pos: 90,
    };
    y = {
        servo: axisY,
        pos: 90
    };

    center();
}

function center() {
    x.pos = 90;
    y.pos = 90;
    x.servo.to(x.pos, 500);
    y.servo.to(y.pos, 500);
    return {x: x.pos, y: y.pos};
}

function left() {
    if (x.pos+stepAngle <= 180) {
        x.pos += stepAngle;
        x.servo.to(x.pos);
    }
    return x.pos;
};
function right() {
    if (x.pos-stepAngle >= 0) {
        x.pos -= stepAngle;
        x.servo.to(x.pos);
    }
    return x.pos;
};
function up() {
    if (y.pos-stepAngle >= 0) {
        y.pos -= stepAngle;
        y.servo.to(y.pos);
    }
    return y.pos;
};
function down() {
    if (y.pos+stepAngle <= 180) {
        y.pos += stepAngle;
        y.servo.to(y.pos);
    }
    return y.pos;
};
