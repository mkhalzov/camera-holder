const servoCtrl = require('./servo-ctrl.js');
const executer = require('./executer.js');
const pwrCtrl = require('./power-ctrl.js');
const coolerCtrl = require('./cooler-ctrl.js');

var exports = module.exports = {};
Object.assign(exports, {
    start
});

const express = require('express');
const app = express();
const hostsConfig = require('../json/hosts.json');

app.get('/hosts', function (req, res) {
    res.status(200).send(hostsConfig);
});
app.get('/left', function (req, res) {
    res.status(200).send({ xPos: servoCtrl.left() });
});
app.get('/right', function (req, res) {
    res.status(200).send({ xPos: servoCtrl.right() });
});
app.get('/up', function (req, res) {
    res.status(200).send({ yPos: servoCtrl.up() });
});
app.get('/down', function (req, res) {
    res.status(200).send({ yPos: servoCtrl.down() });
});
app.get('/center', function (req, res) {
    res.status(200).send({ pos: servoCtrl.center() });
});
app.get('/pwr-on', function (req, res) {
    pwrCtrl.on();
    res.status(200).send(true);
});
app.get('/pwr-off', function (req, res) {
    pwrCtrl.off();
    res.status(200).send(true);
});
app.get('/start-stream', function (req, res) {
    executer.startStream(req.query.hostName).then((success) => {
        res.status(200).send(success);
    }, (response) => {
        res.status(response.status || 500).send(response);
    });
});
app.get('/stop-stream', function (req, res) {
    executer.stopStream(req.query.hostName).then((success) => {
        res.status(200).send(success);
    }, (response) => {
        res.status(response.status || 500).send(response);
    });
});

app.get('/cooler-on', function (req, res) {
    coolerCtrl.on();
    res.status(200).send(true);
});
app.get('/cooler-off', function (req, res) {
    coolerCtrl.off();
    res.status(200).send(true);
});

function start() {
    app.use(express.static('./http'));

    const port = process.env.PORT || 3000;
    app.listen(port);
    console.log(`listening to port ${port}`);
}
