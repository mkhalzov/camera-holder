const { exec } = require('child_process');

const { scriptsPath, scripts } = require('../json/local-config.json');

var exports = module.exports = {};
Object.assign(exports, {
    startStream(hostName) {
        if (scripts[hostName]) {
            let cmd = `${scriptsPath}/${scripts[hostName].startStream}`;
            return run(cmd);
        } else {
            return new Promise((resolve, reject) => {
                reject({
                    status: 404,
                    error: `missing local configuration for ${hostName}`
                });
            });
        }
    },
    stopStream(hostName) {
        if (scripts[hostName]) {
            let cmd = `${scriptsPath}/${scripts[hostName].stopStream}`;
            return run(cmd);
        } else {
            return new Promise((resolve, reject) => {
                reject({
                    status: 404,
                    error: `missing local configuration for ${hostName}`
                })
            });
        }
    }
});

function run(cmd, opts) {
    opts || (opts = {});
    return new Promise((resolve, reject) => {
        let done = false;
        const child = exec(cmd, (error, stdout, stderr) => {
            if (done) {
                console.log('child process executed, but the promise is done', {error, stroud, strerr});
                return ;
            };
            error ? reject(error) : resolve({ stdout, stderr });
        });
        child.on("exit", (code)=>{
            if (done) {
                console.log('child process exited with code %d, but the promise is done', code);
                return ;
            };
            code === 0 ? resolve({ code }) : reject({ code });
        })
        if (opts.stdout) {
            child.stdout.pipe(opts.stdout);
        }
        if (opts.stderr) {
            child.stderr.pipe(opts.stderr);
        }
    })
};
