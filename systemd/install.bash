#!/bin/bash

sudo cp camera-holder.service /etc/systemd/system/
sudo systemctl enable camera-holder.service

#  start, restart and stop service  #
# sudo systemctl start camera-holder.service
# sudo systemctl restart camera-holder.service
# sudo systemctl stop camera-holder.service

#  view service logs  #
# journalctl -u camera-holder
