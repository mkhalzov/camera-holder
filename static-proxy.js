const express = require('express');
const app = express();
const hostsConfig = require('./scripts/hosts.json');

app.get('/hosts', function (req, res) {
    res.status(200).send(hostsConfig);
});

app.use(express.static('./http'));

app.listen(3000);
console.log('listening to port 3000');

